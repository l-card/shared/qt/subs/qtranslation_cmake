if(NOT QTRANSLATION_MACROS_DEFINED__)
    set(QTRANSLATION_MACROS_DEFINED__ ON)
    macro (qtranslation_generate QTRANSLATION_GENFILES QTRANSLATION_INPUT_DIR QTRANSLATION_FILE_BASENAME)
        if(Qt5LinguistTools_FOUND)
            # в Qt универсальные макросы создания документации, не привязанные
            # к старшему номеру версии, были введены только в версии 5.15.
            # Поэтому для версий от Qt5.0 до Qt5.14 включительно используем
            # макросы с префиксом qt5_, а начиная с Qt5.15 - макросы с префиксом qt_
            set(QTRANSLATION_ENABLED TRUE)
            if (${Qt5LinguistTools_VERSION} VERSION_LESS 5.15.0)
                set(QTRANSLATION_QT5_COMPAT TRUE)
            else ()
                set(QTRANSLATION_QT5_COMPAT FALSE)
            endif()
        elseif(Qt6LinguistTools_FOUND)
            set(QTRANSLATION_ENABLED TRUE)
            set(QTRANSLATION_QT5_COMPAT FALSE)
        else()
            # если не найден LinguistTools ни для одной из поддерживаемых
            # макросов, то макрос ничего не выполняет
            # (для возможности использовать макрос независимо от того, требуются
            #  ли проекту файлы перевода, что и определяется по найденному LinguistTools)
            set(QTRANSLATION_ENABLED FALSE)
            set(QTRANSLATION_QT5_COMPAT FALSE)
        endif()


        if (QTRANSLATION_ENABLED)
            # определения значений параметров:
            # - сперва проверяем наличие переменной с заданным QTRANSLATION_FILE_BASENAME
            # - если не определена, проверяем наличие глобальной переменной
            # - если и она не определена, устанавливаем значение по умолчанию

            #разрешение генерации/обновления .ts файлов по исходным текстам программы
            if(DEFINED QTRANSLATION_${QTRANSLATION_FILE_BASENAME}_UPDATE_ENABLE)
                set (QTRANSLATION_CURRENT_UPDATE_ENABLE ${QTRANSLATION_${QTRANSLATION_FILE_BASENAME}_UPDATE_ENABLE})
            elseif(DEFINED QTRANSLATION_GLOBAL_UPDATE_ENABLE)
                set (QTRANSLATION_CURRENT_UPDATE_ENABLE ${QTRANSLATION_GLOBAL_UPDATE_ENABLE})
            else()
                set (QTRANSLATION_CURRENT_UPDATE_ENABLE FALSE)
            endif()

            if(DEFINED QTRANSLATION_${QTRANSLATION_FILE_BASENAME}_LANGUAGES)
                set (QTRANSLATION_CURRENT_LANGUAGES ${QTRANSLATION_${QTRANSLATION_FILE_BASENAME}_LANGUAGES})
            elseif(DEFINED QTRANSLATION_GLOBAL_LANGUAGES)
                set (QTRANSLATION_CURRENT_LANGUAGES ${QTRANSLATION_GLOBAL_LANGUAGES})
            else()
                set (QTRANSLATION_CURRENT_LANGUAGES ru)
            endif()

            if(DEFINED QTRANSLATION_${QTRANSLATION_FILE_BASENAME}_BINARY_DIR)
                set (QTRANSLATION_CURRENT_BINARY_DIR ${QTRANSLATION_${QTRANSLATION_FILE_BASENAME}_BINARY_DIR})
            elseif(DEFINED QTRANSLATION_GLOBAL_BINARY_DIR)
                set (QTRANSLATION_CURRENT_BINARY_DIR ${QTRANSLATION_GLOBAL_BINARY_DIR})
            else()
                set (QTRANSLATION_CURRENT_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/translations)
            endif()


            set(QTRANSLATION_LANGUAGE_TS_FILES)
            set(QTRANSLATION_QM_FILES)

            # определяем список ts-файлов (по файлу на каждый язык)
            foreach(LANGUAGE ${QTRANSLATION_CURRENT_LANGUAGES})
                set(QTRANSLATION_TS_FILE "${QTRANSLATION_INPUT_DIR}/${QTRANSLATION_FILE_BASENAME}_${LANGUAGE}.ts")
                set_source_files_properties(${QTRANSLATION_TS_FILE} PROPERTIES OUTPUT_LOCATION ${QTRANSLATION_CURRENT_BINARY_DIR})
                set(QTRANSLATION_LANGUAGE_TS_FILES ${QTRANSLATION_LANGUAGE_TS_FILES} ${QTRANSLATION_TS_FILE})
            endforeach(LANGUAGE ${QTRANSLATION_CURRENT_LANGUAGES})

            if(QTRANSLATION_CURRENT_UPDATE_ENABLE)
                #список исходных файлов для получения строк для перевода определяется
                #произвольным числом дополнительных параметров макроса
                set(QTRANSLATION_SOURCES )
                foreach(QTRANSLATION_SOURCE ${ARGN})
                    set(QTRANSLATION_SOURCES ${QTRANSLATION_SOURCES} ${QTRANSLATION_SOURCE})
                endforeach()

                if (QTRANSLATION_QT5_COMPAT)
                    qt5_create_translation(QTRANSLATION_QM_FILES
                        ${QTRANSLATION_LANGUAGE_TS_FILES}
                        ${QTRANSLATION_SOURCES})
                else(QTRANSLATION_QT5_COMPAT)
                    qt_create_translation(QTRANSLATION_QM_FILES
                        ${QTRANSLATION_LANGUAGE_TS_FILES}
                        ${QTRANSLATION_SOURCES})
                endif(QTRANSLATION_QT5_COMPAT)
            else()
                if (QTRANSLATION_QT5_COMPAT)
                    qt5_add_translation(QTRANSLATION_QM_FILES ${QTRANSLATION_LANGUAGE_TS_FILES})
                else(QTRANSLATION_QT5_COMPAT)
                    qt_add_translation(QTRANSLATION_QM_FILES ${QTRANSLATION_LANGUAGE_TS_FILES})
                endif(QTRANSLATION_QT5_COMPAT)
            endif()

            set(${QTRANSLATION_GENFILES} ${${QTRANSLATION_GENFILES}} ${QTRANSLATION_QM_FILES})
        endif(QTRANSLATION_ENABLED)
    endmacro()
endif()
